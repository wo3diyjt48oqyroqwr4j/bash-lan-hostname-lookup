#!/bin/bash
LOOKUP_DEP="nslookup"
if [[ ! -x "$(command -v ${LOOKUP_DEP})" ]] ; then
    echo "this script requires ${LOOKUP_DEP}
Install on arch:
 pacman -Sy dnsutils
Other Distros:
 Move to arch!! 
"
fi
if [[ -z "${1}" ]] ; then
    echo "This script looks for lan hostnames
usage of ${0##*/}
    ${0##*/} <ip prefix>

Example:
    ${0##*/} 10.0.0.
"
    exit 2
fi
for (( i = 2 ; i < 256 ; ++i )) ; do
    RETURN_VALUE=$(${LOOKUP_DEP} ${1}${i})
    if [[ ! "${RETURN_VALUE}" =~ "server can't find" ]] ; then
        FOUND_HOSTNAME="$(cut -d"=" -f 2 <<<${RETURN_VALUE})"
        echo "${1}${i} -> ${FOUND_HOSTNAME: : -1}"
    fi
done 
